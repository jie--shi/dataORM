/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import { BaseDao, DaoSession, GlobalContext, OnTableChangedListener, Property, TableAction } from '@ohos/dataorm';
import promptAction from '@ohos.promptAction';
import dataRdb from '@ohos.data.relationalStore';
import { Phone } from './entry/Phone';

@Entry
@Component
struct InsertPage {
  @State message: string = 'Hello World';
  private daoSession: DaoSession | null = null;
  private phoneDao: BaseDao<Phone, number> | null = null;
  private mPhone: Phone | null = null;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text(this.message).fontSize(20)
      Button('插入一条Phone数据').fontSize(20).fontWeight(FontWeight.Bold).margin({ top: 20 }).onClick(() => {
        this.addData();
      })
      Button('更新一条Phone数据').fontSize(20).fontWeight(FontWeight.Bold).margin({ top: 20 }).onClick(() => {
        this.updateData();
      })
      Button('查询所有Phone数据').fontSize(20).fontWeight(FontWeight.Bold).margin({ top: 20 }).onClick(() => {
        this.queryData();
      })
      Button('批量新增').fontSize(20).fontWeight(FontWeight.Bold).margin({ top: 20 }).onClick(() => {
        this.addArrayData();
      })
      Button('删除名字为OPPO的Phone数据').fontSize(20).fontWeight(FontWeight.Bold).margin({ top: 20 }).onClick(() => {
        this.deleteData();
      })
      Button('批量新增或更新').fontSize(20).fontWeight(FontWeight.Bold).margin({ top: 20 }).onClick(() => {
        this.updateArrayData();
      })
      Button('条件查询').fontSize(20).fontWeight(FontWeight.Bold).margin({ top: 20 }).onClick(() => {
        this.queryData2();
      })
    }
    .width('100%')
    .height('100%')
  }

  async addArrayData() {
    let phone1 = new Phone(1, "华为", 52);
    let phone2 = new Phone(2, "iphone", 51);
    let phone3 = new Phone(3, "小米", 50);
    let phone4 = new Phone(4, "OPPO", 40);
    let phone5 = new Phone(5, "VIVO", 45);

    let array = new Array<Phone>();
    array.push(phone1, phone2, phone3, phone4, phone5);
    if (this.phoneDao) {
      await this.phoneDao.insertOrReplaceInTxArrAsync(array);
    }
  }

  async updateArrayData() {
    let phone1 = new Phone(1, "华为", 20);
    let phone2 = new Phone(2, "iphone", 20);
    let phone3 = new Phone(3, "小米", 15);
    let phone4 = new Phone(4, "OPPO", 25);
    let phone5 = new Phone(5, "VIVO", 50);
    let phone6 = new Phone(6, "锤子", 66);

    let array = new Array<Phone>();
    array.push(phone1, phone2, phone3, phone4, phone5, phone6);
    if (this.phoneDao) {
      await this.phoneDao.insertOrReplaceInTxArrAsync(array);
    }
  }

  addData() {
    if (!this.mPhone) {
      this.mPhone = new Phone(1, "iphone", 10);
    }
    if (this.phoneDao) {
      this.phoneDao.insert(this.mPhone);
    }
  }

  updateData() {
    if (this.mPhone) {
      this.mPhone = new Phone(1, "iphone", 20);
      if (this.phoneDao) {
        this.phoneDao.update(this.mPhone);
      }
    }
  }

  async queryData() {
    if (!this.phoneDao) {
      return;
    }
    let entityClass = GlobalContext.getContext().getValue(GlobalContext.KEY_CLS) as Record<string, Object>;
    let properties = entityClass.Phone as Record<string, Property>;
    let query = this.phoneDao.queryBuilder().orderAsc(properties.id).buildCursor();
    let a = await query.list();
    this.message = JSON.stringify(a);
  }

  async queryData2() {
    if (!this.phoneDao) {
      return;
    }

    let entityClass = GlobalContext.getContext().getValue(GlobalContext.KEY_CLS) as Record<string, Object>;
    let properties = entityClass.Phone as Record<string, Property>;
    let query1 = this.phoneDao.queryBuilder();
    let query2 = this.phoneDao.queryBuilder();
    let result = this.phoneDao.queryBuilder();

    let condition1 = query1.and(properties.id.eq(1), properties.phonePrice.eq(20), []);
    let condition2 = query2.and(properties.id.eq(1), properties.phoneName.eq("iphone"), []);
    result.or(condition1, condition2, []);

    let a = await result.list();
    this.message = JSON.stringify(a);
  }

  async deleteData() {
    if (!this.phoneDao) {
      return;
    }
    let entityClass = GlobalContext.getContext().getValue(GlobalContext.KEY_CLS) as Record<string, Object>;
    let properties = entityClass.Phone as Record<string, Property>;
    let deleteQuery = this.phoneDao.queryBuilder().where(properties.phoneName.eq("OPPO"))
      .buildDelete();
    deleteQuery.executeDeleteWithoutDetachingEntities();
  }

  aboutToAppear() {
    this.daoSession = GlobalContext.getContext().getValue("daoSession") as DaoSession;
    this.phoneDao = this.daoSession.getBaseDao(Phone);
    this.phoneDao.addTableChangedListener(this.tabListener());
  }

  tabListener(): OnTableChangedListener<dataRdb.ResultSet> {
    return {
      async onTableChanged(t: dataRdb.ResultSet, action: TableAction) {
        if (action == TableAction.INSERT) {
          promptAction.showToast({ message: "数据添加成功" });
        } else if (action == TableAction.UPDATE) {
          promptAction.showToast({ message: "数据更改成功" });
        } else if (action == TableAction.DELETE) {
          promptAction.showToast({ message: "数据删除成功" });
        } else if (action == TableAction.QUERY) {
          promptAction.showToast({ message: "数据查询成功" });
        }
      }
    }
  }
}

